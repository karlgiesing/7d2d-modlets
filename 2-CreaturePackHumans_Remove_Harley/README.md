# 2-CreaturePackHumans_Remove_Harley

Removes the Harley Quinn NPC from all entity groups.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `0-CreaturePackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

This modlet is also compatible with the `1-NPCPackHumans` modlet.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet uses the XPath `remove` command to remove Harley Quinn entities from all entity groups.

This will stop new Harley Quinn NPCs from spawning.
But it will *not* remove existing Harley Quinn NPCs from the game.
For this reason, *starting a new game* is suggested after this modlet is installed.

The modlet removes all entites whose names start with "humanHarley" (the `0-CreaturePackHumans`
standard) or "NPCHarley" (the `1-NPCPackHumans` standard).

If you do not have the `1-NPCPackHumans` modlet installed, you will probably get console warnings
about the XPath command not having any effect.
These warnings can be ignored.
