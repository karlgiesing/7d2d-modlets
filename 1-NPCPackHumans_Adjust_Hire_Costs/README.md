# 1-NPCPackHumans_Adjust_Hire_Costs

Adjusts the hire costs of human NPCs.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `1-NPCPackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet uses the XPath `set` command to set the `HireCost` property of NPC entity classes.

This will immediately adjust the hire costs of NPCs *when they are first hired.*
It will not have any effect on NPCs that are already hired.
(Sorry, no refunds.)

If this is acceptable, it *should not* be necessary to start a new game after installing this
modlet.

### How hire costs are calculated 

The hire costs are based on the adjusted health, stamina, and damage resistance levels from the
`1-NPCPackHumans_Adjust_Health_Stamina` modlet.
But, given the nature of the NPCs, they should just "make sense" even without that modlet.

I tried to scale the hire costs such that the differences between NPCs are not trivial,
and so that the contributions from each NPC's attributes are balanced.
This took a lot of trial and error.
The formula I ended up with is this:
```
cost_for_health = round_up_to_nearest_50(HealthMax^2 * 0.08);
cost_for_damage = round_up_to_nearest_50((PhysicalDamageResist * Mobility)^2 * 0.6);
total = base_cost + cost_for_health + cost_for_damage
```

In Javascript:
```javascript
const baseCost = 200;
const healthMultiplier = .08;
const damageMultiplier = .6;

function roundUpToNearestMultiple(num,  mult) {
    return mult * Math.ceil(num / mult);
}

function calculateAttributeCost(attribute, multiplier) {
    return roundUpToNearestMultiple(
        attribute * attribute * multiplier,
        50)
}

function calculateCost(healthMax, damageResist, mobility) {
    return baseCost +
        calculateAttributeCost(healthMax, healthMultiplier) +
        calculateAttributeCost(damageResist * mobility, damageMultiplier);
}


console.log(`NPCBaker:           ${calculateCost(50, 22, 0.92)}`);
console.log(`NPCEve:             ${calculateCost(75, 32.4, 0.87)}`);
console.log(`NPCHeavyGuy:        ${calculateCost(100, 44, 0.85)}`);
console.log(`NPCCowboy:          ${calculateCost(75, 29, 0.96)}`);
console.log(`NPCJoel:            ${calculateCost(50, 20, 0.96)}`);
console.log(`NPCHunter:          ${calculateCost(50, 13, 0.96)}`);
console.log(`NPCDelta:           ${calculateCost(125, 52, 0.85)}`);
console.log(`NPCNationalGuard:   ${calculateCost(100, 39, 0.88)}`);
console.log(`NPCRanger:          ${calculateCost(150, 65, 0.82)}`);
console.log(`NPCArmyDoctor:      ${calculateCost(50, 13, 0.96)}`);
console.log(`NPCGhost:           ${calculateCost(75, 37, 0.88)}`);
console.log(`NPCRaiderPest:      ${calculateCost(50, 15.6, 1)}`);
console.log(`NPCRaiderMoto:      ${calculateCost(100, 46, 0.85)}`);
console.log(`NPCRaiderSlugger:   ${calculateCost(50, 15.6, 1)}`);
console.log(`NPCRaiderRat:       ${calculateCost(50, 15.6, 1)}`);
console.log(`NPCRaiderHumongous: ${calculateCost(50, 22.4, 0.9)}`);
console.log(`NPCRaiderJayne:     ${calculateCost(25, 9, 1)}`);
console.log(`NPCGupSoldier1:     ${calculateCost(150, 71, 0.78)}`);
console.log(`NPCGupSoldier6:     ${calculateCost(125, 55, 0.83)}`);
console.log(`NPCGupSoldier7:     ${calculateCost(150, 69, 0.78)}`);
console.log(`NPCGupSoldier8:     ${calculateCost(125, 55, 0.83)}`);
```

### Possible future changes

* Different NPCs could also be hired with something other than Dukes;
  medical NPCs could be hired with antibiotics or medical kits,
  soldier NPCs could be hired with ammo, etc.
* Hire costs could be adjusted by weapon type.
  This is not considered because the weapon type does not make much difference right now,
  but may be once weapon damages and ranged attacks are worked out.
