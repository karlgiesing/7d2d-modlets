# khzmusik A18 UMA Zombies

**Warning: UMA entities are not as performant as vanilla zombie entities.**
It is probably best if there are no more than a handful of them in the world at the same time.

These are zombie versions of the vanilla UMA archetypes.
Since the archetypes already exist in the vanilla code, they are less likely to break between
updates, and should be relatively easy to fix if they do.

This is an XML-only modlet, so it should be EAC compatible, and is almost certainly compatible
with other modlets that add other zombies or NPCs.

_Note: I did not do a version of Madmole, since he would never, ever die and turn into a zombie._

## XML Naming conventions

I chose names that are consistent with the vanilla XML names, but have "KHz" or "Khz" in them.
Hopefully these names should not conflict with any other UMA zombie modlets.

* For new archetypes, I prefixed "KHzZombie" to the name of the vanilla archetype.
* For new entity classes, I prefixed "ZombieKhz" to the name of the vanilla archetype.
  (The reason it is not "KHz" is because the spawning UI, in debug mode, automatically puts
  spaces between capital letters, so it would show as e.g. "Zombie K Hz Aiden".)

## UMA entity classes

The entity classes are based upon `zombieUMATemplateMale` and `zombieUMATemplateFemale`.
These already exist in the vanilla code but are commented out.
I did comment some tags from the vanilla code, in order for them to not be lootable,
and instead to have a small chance to drop loot bags.
This is purely for consistency with the vanilla zombies.

The zombies' movement and animations are controlled by the vanilla `AvatarZombieUMAController`.
This modlet *should* work as long as that controller still works in the vanilla game.
However, The Fun Pimps may remove that controller at any time, and if they do,
this modlet will no longer function.

(For the curious, this is one reason why friendly NPCs and bandits don't work any more;
their `AvatarUMAController` was either broken or removed in Alpha 17.)

## Zombie makeover

For consistency, and to get this done quickly, I used a standardized "makeover" workflow,
so each archetype looks like a dead version of its living self.

Most changes were to the `<base_mesh_parts>` nodes.
The `<expression>` and `<dna>` nodes were not modified.

### Skin color
The intent was to "zombify" the skin colors of existing avatar skin colors.
This ended up being more difficult than you would think.

The colors of living UMA avatar skins, like `male_head` or `female_head`, are not pure white.
So, if you put `skin_color="255,255,255"` in the XML, the in-game skin color will be closer to
rgb(255, 161, 151).
To get close to "grey" in-game you would set `skin_color="151,215,255"` in the XML.
It still isn't pure grey, but it's about as close as you can get.
It's also the lightest "grey" skin color you can get, since you can't specify values above 255.

But most NPC skin colors aren't set to `skin_color="255,255,255"` in the XML.
And, even the darkest-skinned avatars (like Willie) don't use `skin_color="0,0,0"`.
These are the ranges of all avatar `skin_color` values:
```
R: 64 .. 255 => 191
G: 48 .. 255 => 207
B: 43 .. 255 => 212
```

Using the minimum values from the avatar `skin_color` attribute as minimums, 
and the values from RGB "grey" as maximums,
we get these ranges:
```
R: 64 .. 151 => 87
G: 48 .. 215  => 167
B: 43 .. 255  => 212
```

Because zombies should generally be paler than usual, I halved these values to get the new RGB ranges:
```
R: 107 .. 151 => 44
G: 131 .. 215  => 84
B: 149 .. 255  => 106
```

This means the zombified skin colors will have far less range than their "live" NPC counterparts.
But, all the other methods I tried resulted in zombies that became too blue-shifted as their
original skin color darkened.

#### Algorithm
Normalize the original RGB value into a range of 0..1:
```
normalized[R,G,B] = (orig[R,G,B] - min(skin_color[R,G,B])) / (range(skin_color[R,G,B]))
```

Now put it into our new zombie RGB range:
```
new_skin_value[R,G,B] = (normalized[R,G,B] * range(zombified[R,G,B])) + min(zombified[R,G,B])
```

For the lazy, like me, here's some JS that does the work and prints the result to the console:
```javascript
const avatar = [64,48,45]; // Change this to the avatar skin_color you want to "zombify"

const sMin   = [64, 48, 43];    // original skin_color minimum RGB values
const sRange = [191, 206, 212]; // original skin_color RGB ranges

const zMax   = [151, 215, 255];  // zombified skin max RGB values ("grey")
const zRange = [44, 84, 106];    // zombified skin RGB ranges

// Calculate the zombie skin min RGB values from max and range
const zMin   = zMax.map((max, i) => max - zRange[i]);

let rgb = avatar.map((c, i) => {
  let norm = (c - sMin[i]) / (sRange[i]);
  let z = (norm * zRange[i]) + zMin[i];
  return Math.ceil(z);
});

let msg = `${rgb[0]},${rgb[1]},${rgb[2]}`;

console.log(msg);
```

### Clothing

The vanilla archetypes are designed to be player characters that spawn without clothing, and with
a backpack.

So, I took their clothing from the archetypes' `<preview_mesh_parts>` nodes.
This is the clothing you see when previewing the archetype in the NPC creator screen,
and seemed appropriate.
Afterwards, all child nodes in `<preview_mesh_parts>` were removed, as they are not seen in-game.

Since zombie colors should be faded and dirty, I chose a "brown" web-safe color as a baseline:
```
rgb(153, 102, 51) (hex #996633)
```

The final color is the midpoint between that value and the original value:
```
final = ((baseline - original) / 2) + original // (rounding up if necessary)
```

### `<base_mesh_parts>` changes

* NPC heads are replaced with zombie heads of the appropriate gender

* Zombies' heads have head wounds:
  * Female: `<texture name="female_head_wound" color="72,23,16,255" />`
  * Male: `<texture name="male_head_wound" color="72,23,16,255" />`

  _Note: There are a few zombies where this doesn't work, and the wound is transparent._
  _I don't know why. Those zombies do not have head wounds._

* Eyes are `<texture name="unisex_eye_bloodshot" color="255,0,0" />`

* Heads, hands, and feet have `<texture name="unisex_face_dirt_overlay" color="100,80,100,255" />`

* Chests and legs have `<texture name="unisex_dirt_overlay" color="100,80,100,255" />`,
  unless they are hidden (e.g. by outer clothing)

* Chests have gender-appropriate wounds:
  * Female: `<texture name="female_torso_wound" color="72,23,16,255" />`
  * Male: `<texture name="male_torso_wound" color="72,23,16,255" />`

* Legs have gender-appropriate wounds:
  * Female: `<texture name="female_legs_wound" color="72,23,16,255" />`
  * Male: `<texture name="male_legs_wound" color="72,23,16,255" />`

* For NPCs with clothing or armor layers above "base", those layers have
  `<texture name="unisex_face_dirt_overlay" color="100,80,100,175" />`
  
  The final value is so the texture is partially see-through, to simulate torn clothing.

* For NPCs with clothing layers above "base", those layers have _gender-inappropriate_ wounds:
  * Female: `<texture name="male_torso_wound" color="126,13,18,200" />`
  * Male: `<texture name="female_torso_wound" color="126,13,18,200" />`
  
  This is also to simulate torn clothing; gender-inappropriateness is so "tears" don't line up with
  wounds on the torso.
  Unfortunately this does not look good on puffer coats worn by females, so those are excepted.

* The back slot, and its backpack mesh, are removed

## Entity class behavior

The vanilla XML sets the base zombie properties as follows:
* `PainResistPerHit`: .45
* `HandItem`: `meleeHandZombie01`
* `ExperienceGain`: 413
* `LootDropEntityClass`: `EntityLootContainerRegular`
* `HealthMax` base: 150
* `HealthMax` perc (zombie HP scale): 1
* `StaminaMax` base: 100
* `PhysicalDamageResist`: 0

These values are set in the `zombieTemplateMale` entity class, which is the base entity class
for the vanilla `zombieUMATemplateMale`.

Those values were set per UMA zombie according to the zombie's physical size, gender, armor, etc.
In many cases the default values were used.

It is not difficult to modify these values.
Add this XML to the UMA zombie's entity class (choosing your own values):
```XML
<!-- these values are from zombieBiker; adjust to your liking -->
<property name="PainResistPerHit" value=".75"/>
<property name="HandItem" value="meleeHandZombieStrong"/>
<property name="ExperienceGain" value="750"/>
<property name="LootDropEntityClass" value="EntityLootContainerStrong"/>
<effect_group name="Base Effects">
  <passive_effect name="HealthMax" operation="base_set" value="280"/>
  <passive_effect name="HealthMax" operation="perc_set" value="1"/>
  <passive_effect name="PhysicalDamageResist" operation="base_set" value="20"/>
  <!-- zombieBiker doesn't do this, but here is where you would set max stamina -->
  <passive_effect name="StaminaMax" operation="base_set" value="280"/>
</effect_group>
```
If that XML is already there, it should be easy to modify using XPath.

_NOTE: Many entity classes set the `PhysicalDamageResist` passive effect._
_But according to comments in the XML for `zombieFootballPlayer`, this doesn't work "because reasons."_
_I assume that refers only to `zombieFootballPlayer`, since it's the only entity with that comment._

## Spawning

I have added the zombies to existing spawn groups, as I thought appropriate.
You may disagree with my version of "appropriate," and that is perfectly fine.

This was done by adding them to any entity group that already had relevant vanilla zombies.
They intentionally have a lower chance of spawining than vanilla zombies.
This is done partly for performance reasons, and partly because they represent zombified
post-apocalypse survivors, and survivors should be rare (IMHO).

## Improvements?

These are possible but I'm not really interested in doing them at the moment.
Other modders are more than welcome to improve upon this modlet themselves.
If there is a large demand then I might consider doing them in the future.

* Feral versions of some or all UMA zombies
* Radiated versions of some or all UMA zombies
* Specialized spawn groups/entity groups (that spawn only these UMA zombies and not vanilla)

## Additional information for other modders

### Wound overlays
These were discovered by looking through the `resources.assets` file using 
[Unity Assets Bundle Extractor](https://7daystodie.com/forums/showthread.php?22675-Unity-Assets-Bundle-Extractor).

* `female_head_wound`
* `female_legs_wound`
* `female_torso_wound`
* `male_head_wound`
* `male_legs_wound`
* `male_torso_wound`

### StompyNZ info
StompyNZ put together a list of wearable items when creating the original Bad Company NPCs:
> [Page 3 of Bad Company thread on 7D2D forums](https://7daystodie.com/forums/showthread.php?52099-Bad-Company/page3)

Much of that data is probably out of date, since it was written in 2016.
Make sure you test anything that is listed there.
I am linking to it for information only.
