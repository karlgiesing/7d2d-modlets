# 7D2D Modlets

Modlets (small modifications) for the game 7 Days To Die (aka 7D2D):
[7daystodie.com](https://7daystodie.com/)

I am user [khzmusik](https://community.7daystodie.com/profile/51265-khzmusik/) on the 7D2D forums.

## Installation

These modlets were created for, and tested with, Alpha 18.
They are unlikely to work with previous major alpha versions.

As The Fun Pimps (creators of 7D2D) make new versions available, I will try my hardest to update
the modlets to work with the new versions.
Details are TBD, but I will also try to keep modlets available that work with previous versions.

These modlets use XML and XPath, and do not require SDX/DMT or changes to any .dll files.
They may be used with or without EAC enabled, and may be installed on any multi-player server.

However, many are dependent upon other modlets (such as the modlets that add NPCs).
While my modlets do not require SDX/DMT, they may be dependent upon other modlets that do.
Make sure you understand the requirements of these dependent modlets before installing mine.

Modlets now have individual README files.
Under the technical details secion, it should tell you whether you need to start a new game world
or not.
But if you want to be cautious, start a new game world after installing *any* modlets.
The README files should also contain details about modlet dependencies.

### Installation using the Mod Launcher

Using the Mod Launcher is now the recommended way to install these modlets.
In addition to being easier to work with,
it also automatically installs modlet dependencies,
can automatically update any installed modlets,
and will transparently compile any modlet code that requires SDX or DMT.

The Mod Launcher can be downloaded here:

> [7d2dmodlauncher.org](http://7d2dmodlauncher.org)

Since these modlets are included in the Mod Launcher database, you can follow these instructions:

> [Managing Modlets](http://7d2dmodlauncher.org/ManagingModlets.html)

I recommend creating a new My Mods, and installing modlets into that;
this keeps the My Mods game files completely separate from the vanilla game.

To create a new My Mods:

1. In the Mod Launcher, open `File > Add New My Mods`.
1. Enter a name of your choice into the "My Server Name" field.
   This will end up being a folder name on your system.
1. Click "Save". This will save the configuration for the new My Mod, but it won't be installed.
1. Navigate to the My Mod you created. It should be under "My Mods" in the navigation pane on the left.
1. Install the new My Mod by clicking "Install Game Copy".
   It is recommended to use the "Copy from an existing copy" option.
   (Note: this will take a while, be patient.)
1. Follow the "Managing Modlets" instructions, above, to install whichever modlets you desire.
   Filter by author "khzmusik" to see only my modlets.
1. Run the game by clicking the "Play Mod" button in the Mod Launcher.

### Installation into the "vanilla" 7D2D game folder

With this method, you will download the source code from the GitLab site,
and move the code directly into your 7D2D game folder.

1. Open the GitLab page for this project:
   > [https://gitlab.com/karlgiesing/7d2d-modlets](https://gitlab.com/karlgiesing/7d2d-modlets)
1. Make sure you are on the master branch (by default, you should be).
1. On the middle right-hand side of the page, below the description, will be a download button;
   click it and select the compression type of your choice (e.g. "zip" for Windows users).
1. Download the compressed file to a directory of your choice.
1. Uncompress the file. This will create a folder called `7d2d-modlets-master`.
1. Open up your 7D2D game folder. On Windows, it is usually located here:
   > `C:\Program Files (x86)\Steam\steamapps\common\7 Days To Die`
1. Create a `Mods` folder if one does not already exist.
1. Move any desired modlets from the `7d2d-modlets-master` directory into the game's `Mods` folder.

The next time the game is started, these modlets will be active.

### Installation into the Mod Launcher's MyMods game folder

You should only need to do this if the modlet is not (yet) displayed in the mod launcher.
This only applies to *new* modlets - updates to existing modlets should be handled automatically.
New modlets should automatically show up, but there is often a significant delay before they do.

1. Create a new "My Mods" as detailed above, but stop at the "Manage Modlets" step.
   This will create an entirely new game folder (by default, in `C:\7D2D\AlphaXX\My_Mods`).
1. Download the source code from the GitLab site, and install it as if you were installing it
   in the vanilla game.
   But instead of using the vanilla 7D2D game folder, use the "My Mods" game folder from the
   previous step.

Once the Mod Launcher "catches up" and recognizes the new modlet, you should be able to install
updates automatically, as if you installed them from the Mod Launcher in the first place.