# 1-NPCPackHumans_Adjust_Health_Stamina

Adjusts the health and stamina of human NPCs to be closer to player values.

For reference, player max health and stamina are each 200.
Additionally, NPCs regenerate health at a rate higher than the player can achieve.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `1-NPCPackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet uses the XPath commands to adjust the NPC health, stamina, and damage resistance.
Because it uses XPath, the values will immediately be adjusted when the game is loaded.
For this reason, it *should not* be necessary to start a new game after installing this modlet.

### How NPCs were changed

For all NPCs, these existing values were initialized:

1. The `HealthMax` passive effect is initially set to 200. 
1. The `StaminaMax` passive effect is initially set to 200.
1. The `PhysicalDamageResist` passive effect is initially set to zero.

Once those values are set to initial values for all human NPCs,
they are individually adjusted per NPC class.

For each NPC class (e.g. "NPCBaker" or "NPCGupSoldier1"), I loaded their models in game,
and tried to match their clothing and armor to vanilla items as best I could.
I then used the stats of the vanilla items to adjust the health, stamina, and damage resistance.
Most NPCs had more than one article of clothing or armor that influenced these values,
though a few NPCs had none.

For each item, the values I recorded were:
* `<passive_effect name="PhysicalDamageResist" operation="base_add" value="[x]"/>`
  (choosing the highest value if there was a range)
* `<passive_effect name="Mobility" operation="perc_add" value="[x]"/>`

The `PhysicalDamageResist` value simply adds to the base value, so you can just add up the values
for individual items to get a final value.

However, it turns out that merely increasing the physical damage resistance doesn't do enough to
differentiate the various NPCs.
So, I also used the value to add to the maximum health of the NPC.

The formula I used is this:
```
HealthMax base_add = Math.ceil((PhysicalDamageResist * 2) / 25) * 25
```
...or in English: take twice the damage resistance, and round it up to the nearest multiple of 25.
This is the value to add to the base maximum health value.

Since each item's `Mobility` value is a percentage decrease, you can't just add them up to get the
final value to use.
Instead, you have to start with an initial mobility value, add the percentage decrease for the item,
use that as the new mobility value, and repeat the process for the next item.
This gives us the final value to set as the base value for the NPC.
To make the in-game calculations simpler, I rounded the final value to two decimal places.

Here's the JavaScript code that I used to do the calculations:
```javascript
// Example values from four items' Mobility perc_add
const mobilityPercAdd = [
  -0.04, // Military Vest
  -0.04, // Military Gloves
  -0.04, // Military Leg Armor
  -0.06, // Steel Boots
];

let mobility = 1;
mobilityPercAdd.forEach((p) => {
  mobility += (mobility * p);
});

// Round to nearest two decimal places to get 0..1
mobility = Math.round(mobility * 100) / 100

console.log(mobility);
```

The `Mobility` value affects the running and walking speeds of the NPC.
Setting it this way - rather than directly setting the running speeds of each NPC - allows for
future-proofing, in case the NPC base speeds are changed in the future.
(It's also a lot easier to do it this way.)

The end result is that NPCs with heavier armor are more powerful, but also slower
(and easier to outrun).
I think this provides a good balance that works for both friendly and non-friendly NPCs.
