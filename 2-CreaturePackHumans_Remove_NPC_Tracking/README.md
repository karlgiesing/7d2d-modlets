# 1-NPCPackHumans_Remove_NPC_Tracking

Removes tracking of human NPCs in the game map.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `0-CreaturePackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

This modlet is also compatible with the `1-NPCPackHumans` modlet.

> **Note:**
> This modlet is not needed with recent versions of `0-CreaturePackHumans` and `1-NPCPackHumans`,
> which have the relevant XML nodes commented out.
> I am leaving this modlet up for prior versions; and in case other modlets (overhauls, future
> versions of the NPC modlets, etc.) un-comment that XML.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet uses the XPath `remove` command to remove these property nodes from all NPC entity
classes:
* `CompassIcon`
* `CompassDownIcon`
* `CompassUpIcon`
* `MapIcon`
* `TrackerIcon`

(Oddly enough, _all_ must be removed - removing only some of them won't work.)

This will immediately remove the ability to track NPCs when the game is loaded.
For this reason, it *should not* be necessary to start a new game after installing this modlet.

The property is removed from all entites whose names start with "human" (the `0-CreaturePackHumans`
standard) or "NPC" (the `1-NPCPackHumans` standard).
Both standards are included purely for forwards compatibility.
Each modlet might not set some (or any) of those properties.

Because of this, you will probably get console warnings about the XPath command not having any effect.
These warnings can be ignored.
