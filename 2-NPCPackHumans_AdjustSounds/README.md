# 2-NPCPackHumans_AdjustSounds

Adjusts the sounds made by human NPCs.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `1-NPCPackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

This modlet is also compatible with the `0-CreaturePackHumans` modlet.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet uses the XPath commands to adjust the NPC health, stamina, and damage resistance.
Because it uses XPath, the values will immediately be adjusted when the game world is loaded.
For this reason, it *should not* be necessary to start a new game after installing this modlet.

Because this modlet adjusts both the CreaturePack and NPCPack NPCs, you will get console warnings
about XPath commands not having any effect.
*These warnings can be ignored.*
(They come from trying to modify XML that is in one pack but not the other.)

### How NPC sounds were changed

The modlet makes these fundamental changes to NPC sounds:

* New `SoundDataNode`s were created for NPC pain and attack sounds, which use vanilla audio clips
  but with adjusted values.
* By default, male and female human NPCs use the vanilla audio clips, or the new NPC versions.
* Only the more aggressive male human NPCs use the "malehate" sound nodes.
* The "malehate" sound nodes have their values adjusted to match the new NPC values.
* Rangers and female raiders again use the sounds that were provided for them by DarkStarDragon.
* For extra creepiness, NPC whisperers use vanilla zombie sounds when wandering.
  (The idea is that you can't easily tell they are in zombie mobs until they attack you.)

The values that were adjusted for NPCs exist mainly to get rid of the "word soup" that happens
when NPCs are fighting with enemies.
This occurred because attack sounds were triggered with each melee swing, pain sounds were
triggered each time the NPC took damage, and both sounds could play simultaneously.

These sound node values were adjusted:

* The `MaxRepeatRate` values were increased.
  _(Note: despite the notes in `sounds.xml` it seems these values are **delay times** - higher_
  _values will result in fewer repeats.)_
* The `<Channel name="Mouth" />` tag was added to each sound node, so they can't play
  simultaneously on the same NPC.
* So (longer) attack sounds would take priority over pain sounds, the `<Priority name="1" />` tag
  was added to attack sound nodes, and the `<Priority name="2" />` tag was added to pain sound
  nodes.
