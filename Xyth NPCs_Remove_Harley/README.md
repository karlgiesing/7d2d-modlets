# Deprecated modlet

This modlet is deprecated.

The original modlet was dependent upon one of the Xyth NPC modlets.
Xyth has since deprecated those modlets in favor of modlets based on Creature Packs.

This is a good idea in my opinion, but it means significant changes to my own modlets,
including renaming them for consistency with the new Creature Pack modlet names.

## Updated equivalent

The updated equivalent to this modlet is:

[2-CreaturePackHumans_Remove_Harley](../2-CreaturePackHumans_Remove_Harley)

Please use that modlet instead.