# Deprecated modlet

This modlet is deprecated.

The original modlet was dependent upon one of the Xyth NPC modlets.
Xyth has since deprecated those modlets in favor of modlets based on Creature Packs.

This is a good idea in my opinion, but it means significant changes to my own modlets,
including renaming them for consistency with the new Creature Pack modlet names.

## Updated equivalent

There currently is no updated equivalent.
This is because there are a vast number of additional NPCs, and deciding where they should spawn
is time-consuming.

Please be patient while I find time to update this modlet.