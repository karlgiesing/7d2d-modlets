# khzmusik_No_Bedrolls

On death, this modlet will reset (not remove) skill points, and remove all books read.
It implements this by triggering the same effect used by Grandpa's Forgetting Elixir.

It is designed to be used with "dead is mostly dead" play styles.
In this play style, when players die, they restart as "new characters."
Unlike "permadeath" or "dead is dead," they spawn into the same world in which they died.