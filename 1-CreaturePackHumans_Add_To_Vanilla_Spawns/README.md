# 1-CreaturePackHumans_Add_To_Vanilla_Spawns

Adds NPCPack humans to vanilla spawn groups.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `0-CreaturePackHumans` modlet.
This dependency is enforced in the `mod.xml` file.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

## Technical Details

This is an XPath/XML modlet, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should push the XML modifications to their clients, so separate client installation
should not be necessary.

This modlet first uses the XPath `remove` command to remove NPCs from the `FriendlyAnimals` group,
as well as any entity groups whose names start with "Zombie."
These are the groups that the `0-CreaturePackHumans` modlet initially placed NPCs in.
(It does *not* remove the entity groups that are dedicated to NPCs, like `SurvivorHoard` (sic),
since these are not used for spawning zombies.)

Once removed, this modlet uses the XPath `append` command to add them back into specific entity
groups that are used for zombie spawns.
Different "flavors" of NPCs are added into different entity groups.

This will affect future spawning, but will *not* remove existing entities from the game.
For this reason, *starting a new game* is suggested after this modlet is installed.

This modlet also adjusts some of the AI targeting done by both humans and zombies.
These changes are necessary to make humans wake up when put into sleeper volumes.
The changes do *not* make the humans spawn in awake; they still spawn in asleep.
They allow humans to wake up *at all* when they encounter the player, zombies, etc.

### The idea behind spawining NPCs with zombies

The primary goal is to associate different NPC types with groups that spawn in
_specific situations._

Examples:
* Different classes of NPCs can show up at different game times.
  For instance, military NPCs might only show up in late game (similar to demolishers).
* NPCs can spawn with weapon types that are appropriate for the game stage.
* Different kinds of NPCs can spawn in different biomes.
  For instance, raiders might only spawn in the wasteland.
* Different kinds of NPCs can spawn in appropriate sleeper volumes.
  Nurses can spawn in hospitals, cowboys in "ghost towns," etc.