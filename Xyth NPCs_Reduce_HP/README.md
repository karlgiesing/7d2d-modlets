# Deprecated modlet

This modlet is deprecated.

The original modlet was dependent upon one of the Xyth NPC modlets.
Xyth has since deprecated those modlets in favor of modlets based on Creature Packs.

This is a good idea in my opinion, but it means significant changes to my own modlets,
including renaming them for consistency with the new Creature Pack modlet names.

## Updated equivalent

The updated equivalent to this modlet is:

[1-NPCPackHumans_Adjust_Health_Stamina](../1-NPCPackHumans_Adjust_Health_Stamina)

Please use that modlet instead.